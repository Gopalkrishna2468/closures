const limitFunctionCallCount = require("../limitFunctionCallCount");

let cb = () => "";
cb = (x) => console.log("cb Invoked");

let nTimes = limitFunctionCallCount(cb, 2);
nTimes.invoke();
nTimes.invoke();