const cacheFunction = require("../cacheFunction");

let cb = () => "";
cb = x => console.log("argument used  " + x);

let argumentUse = cacheFunction(cb);
console.log(argumentUse.invoking("x"));
console.log(argumentUse.invoking("x"));