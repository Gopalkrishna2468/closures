function counterFactory(initial) {
    var currentValue = initial;
    var increment = function (i) {
        currentValue += i;
        console.log('currentValue = ' + currentValue);
    };

    var decrement = function (i) {
        currentValue -= i;
        console.log('currentValue = ' + currentValue);
    }
    return {
        increment: increment,
        decrement: decrement
    };
}

module.exports = counterFactory;