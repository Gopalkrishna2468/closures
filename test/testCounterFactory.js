const counterFactory = require("../counterFactory");

var count = counterFactory(0);
count.increment(1);
count.decrement(2);