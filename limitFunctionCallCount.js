function limitFunctionCallCount(cb, n) {
  let count = n;

  function invoke() {
    count++ < 3 ? cb() : console.log(null);
  }
  return {
    invoke
  };
}

module.exports = limitFunctionCallCount;