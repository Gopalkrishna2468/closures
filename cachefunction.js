function cacheFunction(cb) {
  const cache = new Set();

  function invoking(arg) {
    if (!cache.has(arg)) {
      console.log(cache);
      cache.add(arg);
      return cb(arg);
    } else {
      console.log(cache);
      return cache;
    }
  }
  return {
    invoking
  };
}

module.exports = cacheFunction;